<?php
defined('TYPO3_MODE') || die('Access denied.');


## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$boot = function () {


    ## Add page tsconfig for ce_customer_testimonial
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        ce_customer_testimonial {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Customer Testemonial
                            description = Content Element - Testemonial from a customer
                            tt_content_defValues {
                                CType = ce_customer_testimonial
                            }
                        }
                    }
                    # show := addToList(ce_customer_testimonial)
                }
            }
        }'
    );

    ## Add page tsconfig for ce_customer_testimonials
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        ce_customer_testimonials {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Customer Testimonials
                            description = CE - Testimonials from customers
                            tt_content_defValues {
                                CType = ce_customer_testimonials
                            }
                        }
                    }
                    show := addToList(ce_customer_testimonials)
                }
            }
        }'
    );

    ## Add page tsconfig for ce_video_loop and ce_video
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        ce_video_loop {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Video loop
                            description = CE - Video loop
                            tt_content_defValues {
                                CType = ce_video_loop
                            }
                        }
                        ce_video {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Video
                            description = CE - Video
                            tt_content_defValues {
                                CType = ce_video
                            }
                        }
                    }
                    show := addToList(ce_video_loop,ce_video)
                }
            }
        }'
    );
    ## Add page tsconfig for ce_svg
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        ce_svg {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | SVG
                            description = CE - SVG
                            tt_content_defValues {
                                CType = ce_svg
                            }
                        }
                    }
                    show := addToList(ce_svg)
                }
            }
        }'
    );

    // register hook to hide content elements with unused colPos
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['record_is_used'] [] =
        \KITT3N\Kitt3nContentelements\Hooks\PageLayoutView\PageLayoutViewHook::class . '->contentIsUsed';

    // register for hook to show preview of tt_content element of CType="ce_logo_overview" in page module
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['ce_customer_testimonials'] =
        \KITT3N\Kitt3nContentelements\Hooks\Previews\CeCustomerTestimonialsPreviewRenderer::class;

};

$boot();
unset($boot);