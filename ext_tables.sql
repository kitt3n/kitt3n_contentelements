
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
    tx_kitt3n_contentelements_check smallint(5) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_check_2 smallint(5) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_check_3 smallint(5) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_check_4 smallint(5) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_check_5 smallint(5) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_image int(10) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_image_2 int(10) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_image_3 int(10) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_link varchar(512) DEFAULT '' NOT NULL,
    tx_kitt3n_contentelements_link_2 varchar(512) DEFAULT '' NOT NULL,
    tx_kitt3n_contentelements_link_3 varchar(512) DEFAULT '' NOT NULL,
    tx_kitt3n_contentelements_mn int(11) unsigned DEFAULT '0' NOT NULL,
    tx_kitt3n_contentelements_rte mediumtext COLLATE utf8_unicode_ci,
    tx_kitt3n_contentelements_rte_2 mediumtext COLLATE utf8_unicode_ci,
    tx_kitt3n_contentelements_select varchar(150) DEFAULT '' NOT NULL,
    tx_kitt3n_contentelements_string varchar(150) DEFAULT '' NOT NULL,
    tx_kitt3n_contentelements_string_2 varchar(150) DEFAULT '' NOT NULL
);