<?php

// add the content element to the CType "ce_customer_testimonial"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'KITT3N // Customer Testimonial',
        'ce_customer_testimonial',
        'EXT:kitt3n_contentelements/Resources/Public/Icons/SVGs/ContentElements/ce_customer_testimonial.svg'
    ),
    'CType',
    'kitt3n_contentelements'
);

// add the content element to the CType "ce_customer_testimonials"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'KITT3N // Customer Testimonials',
        'ce_customer_testimonials',
        'EXT:kitt3n_contentelements/Resources/Public/Icons/SVGs/ContentElements/ce_customer_testimonials.svg'
    ),
    'CType',
    'kitt3n_contentelements'
);
// add the content element to the CType "ce_video_loop"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'KITT3N // Video loop',
        'ce_video_loop',
        'EXT:kitt3n_contentelements/Resources/Public/Icons/SVGs/ContentElements/ce_video_loop.svg'
    ),
    'CType',
    'kitt3n_contentelements'
);
// add the content element to the CType "ce_video"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'KITT3N // Video',
        'ce_video',
        'EXT:kitt3n_contentelements/Resources/Public/Icons/SVGs/ContentElements/ce_video.svg'
    ),
    'CType',
    'kitt3n_contentelements'
);
// add the content element to the CType "ce_svg"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'KITT3N // SVG',
        'ce_svg',
        'EXT:kitt3n_contentelements/Resources/Public/Icons/SVGs/ContentElements/ce_svg.svg'
    ),
    'CType',
    'kitt3n_contentelements'
);


// add the default config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    [
        'tx_kitt3n_contentelements_check' => [
            'exclude' => false,
            'label' => 'Check',
            'config' => [
                'type' => 'check',
                'items' => [
                    ['Check 1', ''],
                ]
            ]
        ],
        'tx_kitt3n_contentelements_check_2' => [
            'exclude' => false,
            'label' => 'Check',
            'config' => [
                'type' => 'check',
                'items' => [
                    ['Check 2', ''],
                ]
            ]
        ],
        'tx_kitt3n_contentelements_check_3' => [
            'exclude' => false,
            'label' => 'Check',
            'config' => [
                'type' => 'check',
                'items' => [
                    ['Check 3', ''],
                ]
            ]
        ],
        'tx_kitt3n_contentelements_check_4' => [
            'exclude' => false,
            'label' => 'Check',
            'config' => [
                'type' => 'check',
                'items' => [
                    ['Check 4', ''],
                ]
            ]
        ],
        'tx_kitt3n_contentelements_check_5' => [
            'exclude' => false,
            'label' => 'Check',
            'config' => [
                'type' => 'check',
                'items' => [
                    ['Check 5', ''],
                ]
            ]
        ],
        'tx_kitt3n_contentelements_image' => [
            'exclude' => false,
            'label' => 'Image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'tx_kitt3n_contentelements_image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
            'onChange' => 'reload'
        ],
        'tx_kitt3n_contentelements_image_2' => [
            'exclude' => false,
            'label' => 'Image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'tx_kitt3n_contentelements_image_2',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
            'onChange' => 'reload'
        ],
        'tx_kitt3n_contentelements_image_3' => [
            'exclude' => false,
            'label' => 'Image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'tx_kitt3n_contentelements_image_3',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
            'onChange' => 'reload'
        ],
        'tx_kitt3n_contentelements_link' => [
            'exclude' => false,
            'label' => 'Link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],
        'tx_kitt3n_contentelements_link_2' => [
            'exclude' => false,
            'label' => 'Link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],
        'tx_kitt3n_contentelements_link_3' => [
            'exclude' => false,
            'label' => 'Link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],
        'tx_kitt3n_contentelements_mn' => [
            'exclude' => false,
            'label' => 'Element hinzufügen',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tt_content',
                'foreign_field' => 'tx_kitt3n_contentelements_mn', // should be :: extension accordion table
                'foreign_record_defaults' => [
                    'colPos' => '214',
                    'CType' => 'ce_...' // ToDo: has to be defined!
                ],
                'foreign_sortby' => 'sorting',
                'maxitems'      => 99,
                'appearance' => [
                    'collapse' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ]
        ],
        'tx_kitt3n_contentelements_rte' => [
            'exclude' => false,
            'label' => 'Rich Text Editor',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
            ],
        ],
        'tx_kitt3n_contentelements_select' => [
            'label' => 'Select',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Bitte auswählen', 0],
                    ['Option 1', 1],
                    ['Option 2', 2]
                ]
            ],
        ],
        'tx_kitt3n_contentelements_string' => [
            'exclude' => false,
            'label' => 'String',
            'config' => [
                'type' => 'input',
            ],
        ],
        'tx_kitt3n_contentelements_string_2' => [
            'exclude' => false,
            'label' => 'String 2',
            'config' => [
                'type' => 'input',
            ],
        ],
    ]
);


// add the specific config
$GLOBALS['TCA']['tt_content']['types']['ce_customer_testimonial'] = [
    'showitem' => '
        --div--;Customer Testemonial,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,                        
            tx_kitt3n_contentelements_select,
            tx_kitt3n_contentelements_rte,
            tx_kitt3n_contentelements_string,
            tx_kitt3n_contentelements_string_2,
    ',
    'columnsOverrides' => [
        'tx_kitt3n_contentelements_select' => [
            'label' => 'Bewertung Sterne',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Bewertung abgeben', 0],
                    ['1 Stern', 1],
                    ['2 Sterne', 2],
                    ['3 Sterne', 3],
                    ['4 Sterne', 4],
                    ['5 Sterne', 5],
                ]
            ],
        ],
        'tx_kitt3n_contentelements_rte' => [
            'label' => 'Text (Kunde)',
        ],
        'tx_kitt3n_contentelements_string' => [
            'label' => 'Vor- & Nachname (Kunde)',
        ],
        'tx_kitt3n_contentelements_string_2' => [
            'label' => 'Firmenname (Kunde)',
        ],
    ],
];
$GLOBALS['TCA']['tt_content']['types']['ce_customer_testimonials'] = [
    'showitem' => '
        --div--;Customer Testimonials,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;header,subheader,
            tx_kitt3n_contentelements_mn,
            --div--;Zugriff,
            --palette--;;hidden,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
    ',
    'columnsOverrides' => [
        'tx_kitt3n_contentelements_mn' => [
            'label' => 'Kundenstimme hinzufügen',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tt_content',
                'foreign_field' => 'tx_kitt3n_contentelements_mn',
                'foreign_label' => 'tx_kitt3n_contentelements_string',
                'foreign_record_defaults' => [
                    'colPos' => '214',
                    'CType' => 'ce_customer_testimonial'
                ],
                'appearance' => [
                    'useSortable' => TRUE,
                    'enabledControls' => [
                        'dragdrop' => TRUE
                    ],
                ]
            ]
        ]
    ]
];

$GLOBALS['TCA']['tt_content']['types']['ce_video_loop'] = [
    'showitem' => '
        --div--;Video loop,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            image,assets,bodytext
    ',
    'columnsOverrides' => [
        'assets' => [
            'label' => 'MP4 video',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('assets', [
                'maxitems' => 1,
                'minitems' => 1,
            ], 'mp4')
        ],
        'image' => [
            'label' => 'Poster image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                'maxitems' => 1,
                'minitems' => 1,
            ], 'jpg,jpeg,png')
        ],
        'bodytext' => [
            'label' => 'Ratio',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['16/9', 100 * 9/16],
                    ['21/9', 100 * 9/21],
                    ['21/5', 100 * 5/21],
                    ['4/3', 100 * 3/4],
                    ['3/2', 100 * 2/3],
                    ['1/1', 100 * 1/1],
                ]
            ],
        ],
    ]
];
$GLOBALS['TCA']['tt_content']['types']['ce_video'] = [
    'showitem' => '
        --div--;Video,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            tx_kitt3n_contentelements_check,
            tx_kitt3n_contentelements_check_2,
            tx_kitt3n_contentelements_check_3,
            tx_kitt3n_contentelements_check_4,
            tx_kitt3n_contentelements_check_5,
            tx_kitt3n_contentelements_select,
            image,
            assets,
            bodytext,
            tx_kitt3n_contentelements_rte,
            tx_kitt3n_contentelements_image,
            tx_kitt3n_contentelements_image_2,
            tx_kitt3n_contentelements_image_3
    ',
    'columnsOverrides' => [
        'tx_kitt3n_contentelements_check' => [
            'label' => 'preload',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => 0,
                        1 => 1,
                    ]
                ],
            ]
        ],
        'tx_kitt3n_contentelements_check_2' => [
            'label' => 'autoplay',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => 0,
                        1 => 1,
                    ]
                ],
            ]
        ],
        'tx_kitt3n_contentelements_check_3' => [
            'label' => 'loop',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => 0,
                        1 => 1,
                    ]
                ],
            ]
        ],
        'tx_kitt3n_contentelements_check_4' => [
            'label' => 'muted',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => 0,
                        1 => 1,
                    ]
                ],
            ]
        ],
        'tx_kitt3n_contentelements_check_5' => [
            'label' => 'controls',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => 0,
                        1 => 1,
                    ]
                ],
            ]
        ],
        'tx_kitt3n_contentelements_select' => [
            'label' => 'Typ',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Typ 0', 0],
                    ['Typ 1', 1],
                    ['Typ 2', 2],
                ]
            ],
        ],
        'assets' => [
            'label' => 'MP4 video',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('assets', [
                'maxitems' => 1,
                'minitems' => 1,
            ], 'mp4')
        ],
        'image' => [
            'label' => 'Poster image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                'maxitems' => 1,
                'minitems' => 1,
            ], 'jpg,jpeg,png')
        ],
        'bodytext' => [
            'label' => 'Ratio',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['16/9', 100 * 9/16],
                    ['21/9', 100 * 9/21],
                    ['21/5', 100 * 5/21],
                    ['4/3', 100 * 3/4],
                    ['3/2', 100 * 2/3],
                    ['1/1', 100 * 1/1],
                ]
            ],
        ],
        'tx_kitt3n_contentelements_rte' => [
            'label' => 'Text zum Video',
        ]
    ]
];

$GLOBALS['TCA']['tt_content']['types']['ce_svg'] = [
    'showitem' => '
        --div--;SVG,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,                        
            image
    ',
    'columnsOverrides' => [
        'image' => [
            'label' => 'SVG',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                'maxitems' => 1,
                'minitems' => 1,
            ], 'svg')
        ],
    ]
];