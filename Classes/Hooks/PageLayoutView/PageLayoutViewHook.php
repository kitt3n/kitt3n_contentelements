<?php

namespace KITT3N\Kitt3nContentelements\Hooks\PageLayoutView;


use TYPO3\CMS\Backend\View\PageLayoutView;
/**
 * Class/Function which manipulates the rendering of item example content and replaces it with a grid of child elements.
 *
 * @author Jo Hasenau <info@cybercraft.de>
 * @package TYPO3
 * @subpackage tx_gridelements
 */
class PageLayoutViewHook
{
    public function contentIsUsed(array $params, PageLayoutView $parentObject): bool
    {
        if ($params['used']) {
            return true;
        }
        $record = $params['record'];
        return $record['colPos'] === 214 && !empty($record['tx_kitt3n_contentelements_mn']);
    }
}