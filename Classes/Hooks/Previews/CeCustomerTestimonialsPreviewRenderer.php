<?php

namespace KITT3N\Kitt3nContentelements\Hooks\Previews;

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CeCustomerTestimonialsPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    )
    {
        if ($row['CType'] === 'ce_customer_testimonials') {

            // todo: implement preview code
            $sRows = '';

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
            $statement = $queryBuilder
                ->select('*')
                ->from('tt_content')
                ->where(
                    $queryBuilder->expr()->eq('pid', $row['pid']),
                    $queryBuilder->expr()->eq('colPos', 214),
                    $queryBuilder->expr()->eq('CType', $queryBuilder->createNamedParameter('ce_customer_testimonial')),
                    $queryBuilder->expr()->eq('tx_kitt3n_contentelements_mn', $row['uid'])
                )
                ->execute();

            $iconFactory = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconFactory::class);
            $iconInactive = $iconFactory->getIcon('actions-system-shortcut-new', \TYPO3\CMS\Core\Imaging\Icon::SIZE_SMALL);
            $iconActive = $iconFactory->getIcon('actions-system-shortcut-active', \TYPO3\CMS\Core\Imaging\Icon::SIZE_SMALL);
            $iconRenderInactive = $iconInactive->render();
            $iconRenderActive = $iconActive->render();

            $j = 1;
            while ($aRow = $statement->fetch()) {
                // Do something with that single row

                $sRating = '';
                for($i = 1; $i <= 5; $i++) {
                    if($i <= (int)$aRow['tx_kitt3n_contentelements_select']){
                        $sRating .= $iconRenderActive;
                    } else {
                        $sRating .= $iconRenderInactive;
                    }
                }


                $sRows .=
                    '<div class="col-sm-4" style="margin: 10px 0;">
                        <div class="c-rating">
                            ' . $sRating . ' <span style="margin-left: 5px;">[' . $aRow['tx_kitt3n_contentelements_select'] . '/5]</span>
                        </div>
                        <p>' . $aRow['tx_kitt3n_contentelements_rte'] . '</p>
                        <strong>'. $aRow['tx_kitt3n_contentelements_string'] . ', ' . $aRow['tx_kitt3n_contentelements_string_2'] .'</strong>
                    </div>';

                $j++;
            }


            $sContentHtml = '            
            <div class="container" style="margin-top: 15px; width: 100%;">
                <div class="ce_customer_testimonial_be row"> 
                    <div class="col-sm-12" style="margin-bottom: 10px;">
                        <div style="padding-top: 10px; padding-bottom: 10px; border-bottom: 2px solid #ddd;">
                            <strong>KITT3N Content Element | Kundenbewertung</strong>
                        </div>                               
                    </div>
                    '. $sRows .'
                </div>
            </div>            
            ';

            $itemContent .= $sContentHtml;
            $drawItem = false;

        }
    }
}